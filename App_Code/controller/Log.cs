﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Text;

/// <summary>
/// Summary description for Log
/// 
/// Log.LogError(this.GetType().FullName, MethodBase.GetCurrentMethod().Name, "Error");
/// </summary>
public class Log
{
    private String path { set; get; }
    public String fclass { set; get; }
    public String fmethod { set; get; }
    public String messagehead { set; get; }
    public String messagebody { set; get; }

    public void LogError(String file, String method, String msg)
    {
        try
        {
            GenerateNames();
            GenerateMessage(file, method, msg);

            using (FileStream fs = new FileStream(path, FileMode.Append, FileAccess.Write))
            using (StreamWriter sw = new StreamWriter (fs))
            {
                sw.WriteLine(messagehead);
                sw.WriteLine(messagebody);
                sw.WriteLine("");
            }
        }
        catch (Exception)
        {

        }
    }

    private void GenerateMessage(String file, String method, String msg)
    {
        messagehead = "LOG | " + (DateTime.Now).ToString("HH:mm:ss") + " | " + file + " | " + method + " | ";
        messagebody = "Error: " + msg;
    }

    private void GenerateNames()
    {
        path = @"E:\Inetpub\Inetpub49\SaisonRooster\Citas\Logs" + "LOG" + (DateTime.Now).ToString("dd-MM-yyyy") + ".txt";
        //path = @"S:\LogsTest\Saisonrooster\" + "LOG" + (DateTime.Now).ToString("dd-MM-yyyy") + ".txt";
    }
}
