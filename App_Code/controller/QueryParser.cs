﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Class that will decompose a query, at now the only purpose is
/// to get the first statment of the query and identify the type
/// of query you are trying to do.
/// </summary>
public class QueryParser
{
    public String type; //Set the type of query.
    public String qParsed; //In future: Get the query parsed like a tree.
    //private String rwCommand; //Reserved words (INSERT INTO, SELECT, UPDATE, DELETE).
    //private String rwTable; //Reserved words (FROM).
    //private String rwWhere; //Reserved words (WHERE).
    //private String rwOther; //Reserved words (ORDER BY, GROUP BY).

    ErrorsList ErrorsList = new ErrorsList();
    Log Log = new Log();

    public QueryParser(String query)
    {
        try
        {
            switch (((query.Split(' '))[0]).ToUpper())
            {
                case "INSERT": type = "C";
                    break;
                case "SELECT": type = "R";
                    break;
                case "UPDATE": type = "U";
                    break;
                case "DELETE": type = "D";
                    break;
                default: type = ErrorsList.List(7211);
                    break;
            }
        }
        catch (Exception e)
        {
            Log.LogError(this.GetType().FullName, System.Reflection.MethodBase.GetCurrentMethod().Name, e.ToString());

            type = ErrorsList.List(7210);
        }
    }

    //private String breakQuery(String query, String req)
    //{
    //    String[] parsed;

    //    string[] ssCommand = new string[] { "INSERT INTO", "SELECT", "UPDATE", "DELETE" };
    //    string[] ssTable = new string[] { "FROM" };
    //    string[] ssWhere = new string[] { "WHERE" };
    //    string[] ssOther = new string[] { "ORDER BY", "GROUP BY" };
    //    string[] ssCombo = new string[] { "INSERT INTO", "SELECT", "UPDATE", "DELETE", "FROM", "WHERE", "ORDER BY", "GROUP BY" };

    //    try
    //    {
    //        //breakQueryCommand((query.Split(ssCommand.Concat(ssTable).ToArray(), StringSplitOptions.None))[1]);
    //        //parsed = query.Split(ssCommand, StringSplitOptions.None);
    //        req = query;

    //        parsed = req.Split(ssCombo, StringSplitOptions.None);
    //        req = (parsed[1]).Trim();

    //        //parsed = (parsed[2]).Split(ssTable.Concat(ssWhere).ToArray(), StringSplitOptions.None);
    //        //req += (parsed[1]).Trim();

    //        //parsed = (parsed[2]).Split(ssWhere.Concat(ssOther).ToArray(), StringSplitOptions.None);
    //        //req += (parsed[1]).Trim();
    //    }
    //    catch (Exception)
    //    {
    //        req = "Error";
    //    }

    //    return req;
    //}
}