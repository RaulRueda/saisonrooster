﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Set the properties to decompose the query of any type.
/// </summary>
public class QueryProperties
{
    public String query; //The original query request.
    public String qCommand; //(CRUD) INSERT INTO, SELECT, UPDATE, DELETE
    public String qTable; //Table to manipulate
    public String qWhere; //Where, optional.
    public String qOther; //Other clouse, optional.

	public QueryProperties(String query,
                           String q_command, 
                           String q_table, 
                           String q_where, 
                           String q_other)
	{
        this.query = query;
        this.qCommand = q_command;
        this.qTable = q_table;
        this.qWhere = q_where;
        this.qOther = q_other;
	}
}