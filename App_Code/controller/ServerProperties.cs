﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Set the properties to connect to the server.
/// </summary>
public class ServerProperties
{
    public String dbServer; //Server IP or DNS.
    public String dbHost; //Server IP or Host, optional.
    public String dbPort; //Server port.
    public String dbProtocol; //Server protocol.
    public String dbDatabase; //Database.
    public String dbUser; //Database user.
    public String dbPassword; //Database password.
    public String dbType; //Database type to connect.
    public String resType; //Type of return (text, json, xml).

	public ServerProperties(String db_server, 
                            String db_host,
                            String db_port,
                            String db_protocol, 
                            String db_database, 
                            String db_user, 
                            String db_password, 
                            String db_type, 
                            String res_type)
	{
        this.dbServer = db_server;
        this.dbHost = db_host;
        this.dbPort = db_port;
        this.dbProtocol = db_protocol;
        this.dbDatabase = db_database;
        this.dbUser = db_user;
        this.dbPassword = db_password;
        this.dbType = db_type;
        this.resType = res_type;
	}
}