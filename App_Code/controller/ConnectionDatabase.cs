﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient; //Import to sql server client side.
using IBM.Data.Informix; //Import to access BAAN.

/// <summary>
/// Summary description for ConnectionDatabase
/// </summary>
public class ConnectionDatabase
{
    ServerProperties ServerProperties = new ServerProperties("", "", "", "", "", "", "", "", "");
    Log Log = new Log();

    #region VariablesConnection

    public SqlConnection sqlConnection;
    private String sqlString;

    public IfxConnection ifxConnection;
    private String ifxString;

    #endregion  

    public Boolean Connect()
    {        
        switch (ServerProperties.dbType)
        {
            case "sqlserver":
                sqlString = "Data Source=" + ServerProperties.dbServer.Trim() + ";" +
                            "Initial Catalog=" + ServerProperties.dbDatabase.Trim() + ";" +
                            "User ID=" + ServerProperties.dbUser.Trim() + ";" +
                            "Password=" + ServerProperties.dbPassword.Trim();
                try
                {
                    sqlConnection = new SqlConnection(sqlString);
                    sqlConnection.Open();

                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            case "informix":
                ifxString = "UID=" + ServerProperties.dbUser.Trim() + ";" +
                            "PWD=" + ServerProperties.dbPassword.Trim() + ";" +
                            "DATABASE=" + ServerProperties.dbDatabase.Trim() + ";" +
                            "HOST=" + ServerProperties.dbHost.Trim() + ";" +
                            "SERVER=" + ServerProperties.dbServer.Trim() + ";" +
                            "SERVICE=" + ServerProperties.dbPort.ToString().Trim() + ";" +
                            "PROTOCOL=" + ServerProperties.dbProtocol.Trim() + ";";
                try
                {
                    ifxConnection = new IfxConnection(ifxString); 
                    ifxConnection.Open();

                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            default:
                return false;
        }
    }

    public void Disconnect()
    {
        try
        {
            switch (ServerProperties.dbType)
            {
                case "sqlserver":
                    sqlConnection.Close();
                    break;
                case "informix":
                    ifxConnection.Close();
                    break;
            }
        }
        catch (Exception e)
        {
            Log.LogError(this.GetType().FullName, System.Reflection.MethodBase.GetCurrentMethod().Name, e.ToString());
        }
    }

    public Boolean SetPropertys(String db_server, String db_host, String db_port, String db_protocol, String db_database, String db_user, String db_password, String db_type, String res_type)
    {
        try
        {
            ServerProperties.dbServer = db_server;
            ServerProperties.dbHost = db_host;
            ServerProperties.dbPort = db_port;
            ServerProperties.dbProtocol = db_protocol;
            ServerProperties.dbDatabase = db_database;
            ServerProperties.dbUser = db_user;
            ServerProperties.dbPassword = db_password;
            ServerProperties.dbType = db_type;
            ServerProperties.resType = res_type;

            return true;
        }
        catch (Exception)
        {
            Log.LogError(this.GetType().FullName, System.Reflection.MethodBase.GetCurrentMethod().Name, "Error");

            return false;
        }
    }
}