﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Depends on the number, the class indicates the type of error
/// 
/// The first and second number:
///     - 10 : Main project, saisonrooster.cs
///     - 21 : Controller, QueryCreate.cs
///     - 22 : Controller, QueryRead.cs
///     - 23 : Controller, QueryUpdate.cs
///     - 24 : Controller, QueryDelete.cs
///     - 31 : Controller, ErrorsList.cs
///     - 32 : Controller, ResponseJson.cs
///     - 71 : Model, Sqlserver.cs
///     - 72 : Model, Informix.cs
/// 
/// The third and fourth number of: 
/// 
///     Sainsonrooster.cs (10xx):
///     
///     - 10 : General error.
///     - 11 : Set properties have an error.
///     - 12 : Set query have an error.
///     - 13 : Option of query in builder haven an error.
///     
///     ConnectionDatabase.cs (71xx):
///     
///     - 10 : General error.
///     - 11 : Error trying to connect to server.
///     - 12 : Error trying to disconnect to server.
/// 
///     QueryParser.cs (72xx):
///     
///     - 10 : General error.
///     - 11 : Syntax error, query dont start with a CRUD sentence.
///     
///     ResponseJson.cs (32xx):
///     
///     - 10 : General error.
///    
/// </summary>
public class ErrorsList
{
    public String List(Int32 error)
    {
        switch(error)
        {
            case 1010:
                return "Error: General error code(1010)";
            case 1011:
                return "Error: A problem trying to set the properties of the server, code(1011).";
            case 1012:
                return "Error: A problem trying to set the properties of the query, code(1012).";
            case 1013:
                return "Error: A problem trying to set the builder, code(1013).";
            case 3210:
                return "Error: General error, code(3210).";
            case 3220:
                return "Error: General error in the sql execution, code(3220).";
            case 3221:
                return "Error: The query reached the max timeout of execution, code(3221).";
            case 7110:
                return "Error: General error, code(7110).";
            case 7111:
                return "Error: A problem trying to connect to the server, code(7111).";
            case 7112:
                return "Error: A problem trying to disconnect to the server, code(7112).";
            case 7210:
                return "Error: General error, code(7210).";
            case 7211:
                return "Error: Syntax error, query dont start with a CRUD sentence, code(7211).";
            default:
                return "Error: Error not determined, code(0000).";
        }
    }
}