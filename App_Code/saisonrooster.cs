﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

/// <summary>
/// Summary description for saisonrooster
/// Author: Raul Rueda
/// Contact: raulruedabarajas@gmail.com
/// 
/// This web services was develop using visual studio 2015 community in asp with c#, 
/// the intention is to handle the CRUD sentences by any database responding  
/// (Trying to covert more) and a multiple responde system like Json, Xml and plain Txt. 
/// 
/// If you find this helpful please don’t erase this section, just add your changes 
/// in the section I open. 
/// 
/// <<this section for changes - contributor>> 
/// 
/// Copyright (C) 2016 Raul Alberto Rueda Barajas
/// This program is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
/// 
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
/// 
/// You should have received a copy of the GNU General Public License
/// along with this program.  If not, see <http://www.gnu.org/licenses/>.
/// </summary>
/// 


[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]

[System.Web.Script.Services.ScriptService]
public class saisonrooster : System.Web.Services.WebService
{
    #region Class
    QueryProperties QueryProperties = new QueryProperties("", "", "", "", "");
    ServerProperties ServerProperties = new ServerProperties("", "", "", "", "", "", "", "", "");

    ConnectionDatabase ConnectionDatabase = new ConnectionDatabase();
    ErrorsList ErrorsList = new ErrorsList();
    QueryParser QueryParser;

    ResponseJson ResponseJson = new ResponseJson();

    #endregion

    [WebMethod]
    public String SaisonRooster(String db_server, String db_host, String db_port, String db_protocol, String db_database, String db_user, String db_password, String db_type, String res_type, String query)
    {
        Boolean setProperties = false, setQuery = false;

        setProperties = ConnectionDatabase.SetPropertys(db_server, db_host, db_port, db_protocol, db_database, db_user, db_password, db_type, res_type); //Set all the properties for the current connection.
        ServerProperties.dbType = db_type; //Type of the database will be handle.
        ServerProperties.resType = res_type; //Type of respond.
        setQuery = SetQuery(query); //Set all the properties for the query.

        if (setProperties == true && setQuery == true) //If the sets are true we are ready to continue.
        {
            return Builder("", db_type, res_type);
        }
        else if (setProperties == false)
        {
            return ErrorsList.List(1011);
        }
        else if (setQuery == false)
        {
            return ErrorsList.List(1012);
        }
        else
        {
            return ErrorsList.List(1010);
        }
    }

    #region Properties
    private Boolean SetQuery(String req) //Method to get the type of query we are using of the CRUD.
    {
        try {
            QueryProperties.query = req;
            QueryParser = new QueryParser(req);

            if (QueryParser.type != "ERROR")
            {
                return true;
            }
            else
            {
                return false;
            }
            
        }
        catch (Exception) {
            return false;
        }

    }
    #endregion

    #region Workspace
    private String Builder(String res, String req_db, String req_format) //Method to continue and switch between the CRUD.
    {
        switch (QueryParser.type)
        {
            case "C": //Create
                res = "";
                break;
            case "R": //Read
                res = Read("");
                break;
            case "U": //Update
                res = "";
                break;
            case "D": //Delete
                res = "";
                break;
            default:
                res = ErrorsList.List(1010);
                break;
        }

        return res;
    }
   
    #region Read
    private String Read(String res) //Read method.
    {
        if (ConnectionDatabase.Connect())
        {
            switch (ServerProperties.resType)
            {
                case "string":
                    ConnectionDatabase.Disconnect();

                    return res = "";
                case "json":
                    res = ResponseJson.BuildResponse("", ServerProperties.dbType, QueryProperties.query, ConnectionDatabase);
                    ConnectionDatabase.Disconnect();

                    return res;//Response Json
                case "xml":
                    ConnectionDatabase.Disconnect();

                    return res = "";
                default:  //Defualt always will be just text and comas.
                    ConnectionDatabase.Disconnect();

                    return res = "";
            }
        }
        else
        {
            return res = ErrorsList.List(7111);
        }
    }
    #endregion

    #endregion
}
