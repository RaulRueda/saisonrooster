﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient; //Import to sql server client side.
using IBM.Data.Informix; //Import to access BAAN.
using System.Web.Script.Serialization; //Import Serialization for JSON response.

/// <summary>
/// A class to response a Json demand. 
/// </summary>
public class ResponseJson
{
    ErrorsList ErrorsList = new ErrorsList();
    Log Log = new Log();

    SqlCommand sqlCommand;
    SqlDataReader sqlReader;

    IfxCommand ifxCommand;
    IfxDataReader ifxReader;

    public class Registro
    {
        public String h { set; get; }
        public String b { set; get; }
    }

    Registro rgs = new Registro();

    JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
 
    public string BuildResponse(String res, String req, String query, ConnectionDatabase con)
    { //Cannot use serialize becouse the dynamic number of columns and colum name.
        try
        {
            switch (req)
            {
                case "sqlserver":
                    sqlCommand = con.sqlConnection.CreateCommand(); //Initialize the command with the class ConnectionDatabase.
                    sqlCommand.CommandText = query; //Attach the query to the commando.
                    sqlCommand.CommandTimeout = 600; //Extende timeout for large query or storage precedure.
                    sqlReader = sqlCommand.ExecuteReader(System.Data.CommandBehavior.SingleRow); //Execute the reader for the query sended with the first row.

                    res = "{h: " + BuildSerializeHeadersSqlServer("") + ",";

                    sqlReader.Close(); //To reuse the reader.

                    sqlReader = sqlCommand.ExecuteReader(); //Execute the reader for the query sended.

                    res += "b: " + BuildSerializeBodySqlServer("") + "}";

                    return res; //close the array of objects.
                case "informix":
                    ifxCommand = con.ifxConnection.CreateCommand(); //Initialize the command with the class ConnectionDatabase.
                    ifxCommand.CommandText = query; //Attach the query to the commando.
                    ifxCommand.CommandTimeout = 600; //Extende timeout for large query or storage precedure.
                    ifxReader = ifxCommand.ExecuteReader(System.Data.CommandBehavior.SingleRow); //Execute the reader for the query sended with the first row.

                    res = "{h: " + BuildSerializeHeadersInformix("") + ","; //You can use different method to contruct the json header.

                    ifxReader.Close(); //To reuse the reader.

                    ifxReader = ifxCommand.ExecuteReader(); //Execute the reader for the query sended.

                    res += "b: " + BuildSerializeBodyInformix("") + "}"; //You can use different method to contruct the json body.

                    return res; //close the array of objects.
                default:

                    return res = "[]"; //close the array of objects, with no content. This not suppost to pass becouse the parent validation.
            }
        }
        catch (SqlException exsql) 
        {
            if (exsql.HResult == -2146232060)
            {
                return ErrorsList.List(3221);
            }
            else 
            {
                return ErrorsList.List(3220);
            }
        }
        catch (IfxException exifx) 
        {
            if (exifx.HResult == -2146232060)
            {
                return ErrorsList.List(3221);
            }
            else 
            {
                return ErrorsList.List(3220);
            }
        }
        catch (Exception)
        {
            return ErrorsList.List(3210);
        }
    }

    #region SqlServer Serialize functions
    private String BuildSerializeHeadersSqlServer(String res)
    {
        List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
        Dictionary<string, object> childRow;

        try
        {
            while (sqlReader.Read()) //Read one by one the reader and build a Json object into the array.
            {
                childRow = new Dictionary<string, object>();
                for (int i = 0; i < sqlReader.FieldCount; i++) //Read one by one column of one register to compose the key/value.
                {
                    childRow.Add(sqlReader.GetName(i), sqlReader.GetName(i)); //Use the valor of the column to be the value.
                }
                parentRow.Add(childRow);
            }
        }
        catch (Exception e)
        {
            Log.LogError(this.GetType().FullName, System.Reflection.MethodBase.GetCurrentMethod().Name, e.ToString());
        }

        return jsSerializer.Serialize(parentRow);
    }

    private String BuildSerializeBodySqlServer(String res)
    {
        List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
        Dictionary<string, object> childRow;
        try {
            while (sqlReader.Read()) //Read one by one the reader and build a Json object into the array.
            {
                childRow = new Dictionary<string, object>();
                for (int i = 0; i < sqlReader.FieldCount; i++) //Read one by one column of one register to compose the key/value.
                {
                    if (sqlReader.IsDBNull(i)) //If the valor of column is null will put single quotes.
                    {
                        childRow.Add(sqlReader.GetName(i), "");
                    }
                    else
                    {
                        childRow.Add(sqlReader.GetName(i), (sqlReader.GetValue(i)).ToString().Trim()); //Use the valor of the column to be the value.
                    }
                }
                parentRow.Add(childRow);
            }
        }
        catch (Exception e)
        {
            Log.LogError(this.GetType().FullName, System.Reflection.MethodBase.GetCurrentMethod().Name, e.ToString());
        }

        return jsSerializer.Serialize(parentRow);
    }

    #endregion

    #region Informix Serialize functions
    private String BuildSerializeHeadersInformix(String res)
    {
        List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
        Dictionary<string, object> childRow;

        try
        {
            while (ifxReader.Read()) //Read one by one the reader and build a Json object into the array.
            {
                childRow = new Dictionary<string, object>();
                for (int i = 0; i < ifxReader.FieldCount; i++) //Read one by one column of one register to compose the key/value.
                {
                    childRow.Add(ifxReader.GetName(i), ifxReader.GetName(i)); //Use the valor of the column to be the value.
                }
                parentRow.Add(childRow);
            }
        }
        catch (Exception e)
        {
            Log.LogError(this.GetType().FullName, System.Reflection.MethodBase.GetCurrentMethod().Name, e.ToString());
        }

        return jsSerializer.Serialize(parentRow);
    }

    private String BuildSerializeBodyInformix(String res)
    {
        List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
        Dictionary<string, object> childRow;

        try
        {
            while (ifxReader.Read()) //Read one by one the reader and build a Json object into the array.
            {
                childRow = new Dictionary<string, object>();
                for (int i = 0; i < ifxReader.FieldCount; i++) //Read one by one column of one register to compose the key/value.
                {
                    if (ifxReader.IsDBNull(i)) //If the valor of column is null will put single quotes.
                    {
                        childRow.Add(ifxReader.GetName(i), "");
                    }
                    else
                    {
                        childRow.Add(ifxReader.GetName(i), (ifxReader.GetValue(i)).ToString().Trim()); //Use the valor of the column to be the value.
                    }
                }
                parentRow.Add(childRow);
            }
        }
        catch (Exception e)
        {
            Log.LogError(this.GetType().FullName, System.Reflection.MethodBase.GetCurrentMethod().Name, e.ToString());
        }

        return jsSerializer.Serialize(parentRow);
    }

    #endregion

    #region oldfunctions

    private String BuildStringHeadersSqlServer(String res)  //Works slower than serialize, this time its exponential.
    {
        res = "["; //Open the array of objects.

        while (sqlReader.Read()) //Read one by one the reader and build a Json object into the array.
        {
            res += "{"; //Open the bracket for an object

            for (int i = 0; i < sqlReader.FieldCount; i++) //Read one by one column of one register to compose the key/value.
            {
                res += sqlReader.GetName(i) + ": '" + sqlReader.GetName(i) + "', "; //Use the name of the column to be the key and the value of the object
            }
            res += "}"; //close the bracket for an object

            //break; //Just need one iteration
        }

        return res += "]"; //Close the array of objects.
    }

    private String BuildStringBodySqlServer(String res)  //Works slower than serialize, this time its exponential.
    {
        res = "["; //Open the array of objects.

        while (sqlReader.Read()) //Read one by one the reader and build a Json object into the array.
        {
            res += "{"; //Open the bracket for an object

            for (int i = 0; i < sqlReader.FieldCount; i++) //Read one by one column of one register to compose the key/value.
            {
                res += sqlReader.GetName(i) + ": "; //Use the name of the column to be the key of the object

                if (sqlReader.IsDBNull(i)) //If the valor of column is null will put single quotes.
                {
                    res += "'', ";
                }
                else
                {
                    res += "'" + (sqlReader.GetValue(i)).ToString().Trim() + "', "; //Use the valor of the column to be the value.
                }
            }
            res += "},"; //close the bracket for an object
        }

        return res += "]"; //Close the array of objects.
    }

    private String BuildStringHeadersInformix(String res) //Works slower than serialize, this time its exponential.
    {
        res = "["; //Open the array of objects.

        while (ifxReader.Read()) //Read one by one the reader and build a Json object into the array.
        {
            res += "{"; //Open the bracket for an object

            for (int i = 0; i < ifxReader.FieldCount; i++) //Read one by one column of one register to compose the key/value.
            {
                res += ifxReader.GetName(i) + ": '" + ifxReader.GetName(i) + "', "; //Use the name of the column to be the key and the value of the object
            }
            res += "}"; //close the bracket for an object

            break; //Just need one iteration
        }

        return res += "]"; //Close the array of objects.
    }

    private String BuildStringBodyInformix(String res) //Works slower than serialize, this time its exponential.
    {
        res = "["; //Open the array of objects.

        while (ifxReader.Read()) //Read one by one the reader and build a Json object into the array.
        {
            res += "{"; //Open the bracket for an object

            for (int i = 0; i < ifxReader.FieldCount; i++) //Read one by one column of one register to compose the key/value.
            {
                res += ifxReader.GetName(i) + ": "; //Use the name of the column to be the key of the object

                if (ifxReader.IsDBNull(i)) //If the valor of column is null will put single quotes.
                {
                    res += "'', ";
                }
                else
                {
                    res += "'" + (ifxReader.GetValue(i)).ToString().Trim() + "', "; //Use the valor of the column to be the value.
                }
            }
            res += "},"; //close the bracket for an object
        }

        return res += "]"; //Close the array of objects.
    }

    #endregion
}