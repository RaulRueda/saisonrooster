﻿# SaisonRooster

Summary description for saisonrooster                                                           
Author: **Raul Rueda**                                                                          
Contact: **raulruedabarajas@gmail.com**                                                         
Bitbukcet: **bitbucket.org/RaulRueda/saisonrooster**                                   

This web services was develop using visual studio 2015 community in asp with c#, 
the intention is to handle the CRUD sentences by any database responding  
(Trying to covert more) and a multiple responde system like Json, Xml and plain Txt. 

If you find this helpful please don’t erase this section, just add your changes 
in the section I open. 

<<this section for changes - contributor>> 

Copyright (C) 2016 Raul Alberto Rueda Barajas
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.